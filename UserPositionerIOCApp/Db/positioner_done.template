# Offset setpoint for the Axis X
record(ai, "$(SYS):$(DEV):Offset$(AXIS_NAME)") {
    field(DESC, "Offset - Absolute Pos and User Val")
    field(DTYP, "Soft Channel")
    field(SCAN, "Passive")
    field(PREC, "$(PREC=2)")
    field(FLNK, "$(SYS):$(DEV):UserSetPointRB$(AXIS_NAME)")
}

record(calc, "$(SYS):$(DEV):UserSetPointRB$(AXIS_NAME)") {
    field(DESC, "User Setpoint")
    field(SCAN, "Passive")
    field(CALC, "A-B")
    field(PINI, "YES")
    field(INPA, "$(SYS):$(DEV):PositionRB$(AXIS_NAME) CPP")
    field(INPB, "$(SYS):$(DEV):Offset$(AXIS_NAME) CPP")
}

# User setpoint of the Axis X
record(ai, "$(SYS):$(DEV):UserSetPoint$(AXIS_NAME)") {
    field(DESC, "User Setpoint")
    field(DTYP, "Soft Channel")
    field(SCAN, "Passive")
    #field(PREC, "$(PREC=2)")
    field(DISV, 1)
    field(SDIS, "$(SYS):$(DEV):ActualBusy$(AXIS_NAME).VAL")
    field(FLNK, "$(SYS):$(DEV):SequenceSetPoint$(AXIS_NAME)")
}

record(ao, "$(SYS):$(DEV):OutSetPoint$(AXIS_NAME)") {
    field(OMSL, "closed_loop")
    field(DOL, "$(SYS):$(DEV):SetPointCalc$(AXIS_NAME) PP")
    field(OUT, "$(BEAMLINE_PREFIX):$(SET_NAME)$(AXIS_NAME) PP")
    field(FLNK, "$(SYS):$(DEV):CheckStatus$(AXIS_NAME)")
}

# Compute the absolute displacement
record(calc,"$(SYS):$(DEV):SetPointCalc$(AXIS_NAME)") {
    field(DESC, "Return calc of the absolut displacement")
    field(VAL,  "0")
    field(SCAN, "Passive")
    field(CALC, "(B+A)/C")
    field(INPA, "$(SYS):$(DEV):Offset$(AXIS_NAME)")
    field(INPB, "$(SYS):$(DEV):UserSetPoint$(AXIS_NAME)")
    field(INPC, "$(ASLO)")
}


record(ao, "$(SYS):$(DEV):SequenceSetPoint$(AXIS_NAME)") {
    field(DESC, "Forward SetPoint to multiple dest")
    field(OMSL, "closed_loop")
    field(DOL, "1")
    field(OUT, "$(SYS):$(DEV):SetBusy$(AXIS_NAME) PP")
    field(FLNK, "$(SYS):$(DEV):CalcSelector$(AXIS_NAME)")
}

record(calc, "$(SYS):$(DEV):DetectMov$(AXIS_NAME)") {
    field(DESC, "Detect movement")
    field(CALC, "(ABS(A)==ABS(B-C))?1:0")
    field(INPA, "$(SYS):$(DEV):UserSetPoint$(AXIS_NAME)")
    field(INPB, "$(SYS):$(DEV):PositionRB$(AXIS_NAME)")
    field(INPC, "$(SYS):$(DEV):Offset$(AXIS_NAME)")
}

record(fanout, "$(SYS):$(DEV):CalcSelector$(AXIS_NAME)") {
    field(DESC, "Select which calc to perform")
    field(SELM, "Specified")
    field(SELN, "0")
    field(SELL, "$(SYS):$(DEV):DetectMov$(AXIS_NAME) PP")
    field(LNK0, "$(SYS):$(DEV):OutSetPoint$(AXIS_NAME)")
    field(LNK1, "$(SYS):$(DEV):ClearBusy$(AXIS_NAME)")
}

record(ai, "$(SYS):$(DEV):PositionRBRaw$(AXIS_NAME)") {
    field(DESC, "Current Raw Absolute Position")
    field(DTYP, "Soft Channel")
    field(SCAN, "Passive")
    field(VAL,  "0")
    field(PINI, "YES")
    field(INP, "$(RB_PREFIX):$(POS_RB_NAME)$(AXIS_NAME) CPP")
    field(FLNK, "$(SYS):$(DEV):PositionCalc$(AXIS_NAME)")
}

record(calcout, "$(SYS):$(DEV):PositionCalc$(AXIS_NAME)") {
    field(DESC, "Unit conversion")
    field(SCAN, "Passive")
    field(OOPT, "Every Time")
    field(CALC, "A*B")
    field(INPA, "$(SYS):$(DEV):PositionRBRaw$(AXIS_NAME)")
    field(INPB, "$(ASLO)")
    field(OUT, "$(SYS):$(DEV):PositionRB$(AXIS_NAME) PP")
}

record(ai, "$(SYS):$(DEV):PositionRB$(AXIS_NAME)") {
    field(DESC, "Current Absolute Position")
    field(DTYP, "Soft Channel")
    field(SCAN, "Passive")
    field(VAL,  "0")
}

record(calcout, "$(SYS):$(DEV):CheckStatus$(AXIS_NAME)") {
    field(DESC, "Check Status")
    field(SCAN, "Passive")
    field(CALC, "!(A==$(DONE_VAL))")
    field(INPA, "$(BEAMLINE_PREFIX):$(STATUS_NAME) CPP")
    field(OOPT, "Transition To Zero")
    field(OUT, "$(SYS):$(DEV):ActualBusy$(AXIS_NAME) PP")
}

record(ao, "$(SYS):$(DEV):ClearBusy$(AXIS_NAME)") {
   field(VAL, "0")
   field(DISP, 1)
   field(OMSL, "supervisory")
   field(OUT,  "$(SYS):$(DEV):ActualBusy$(AXIS_NAME) PP")
}

record(ao, "$(SYS):$(DEV):SetBusy$(AXIS_NAME)") {
   field(VAL, "0")
   field(DISP, 1)
   field(OMSL, "supervisory")
   field(OUT,  "$(SYS):$(DEV):ActualBusy$(AXIS_NAME) PP")
}

record(bo, "$(SYS):$(DEV):ActualBusy$(AXIS_NAME)") {
   field(ZNAM, "Done")
   field(ONAM, "Busy")
}

