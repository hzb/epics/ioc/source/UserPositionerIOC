# UserPositionersIOC

This IOC integrates the Optics Beamline's Reflectometer Tripod PVs to provide `setpoint`, `readback` and `done` values for each axis.

## Description

The IOC has two inputs: `Offset` and `UserSetPoint`. The latter represents the relative displacement with respect to the `Offset`. The outputs are: the command to the axis `set` PV on the device and the `done` flag represented by the PV named `ActualBusy`, which is implemented using a `busy` record.

When the operator writes a new value to `UserSetPoint`, the IOC will :

- Compute the absolute displacement (`Offset + USerSetPoint`).
- Set `ActualBusy` flag to `Busy`.
- Set the value to the corresponding axis `set` PV on the device. The movement will start.
- Wait until the Status PV on the device is equal to a configured done value.
- Set `ActualBusy` to `Done`.

If the device does not provide any readback PV for the axis current position, the IOC will save the computed absolute position into `PositionRB`. Otherwise, the value will be retrieved from the device.

In addition, `UserSetPointRB` provides the readback for the relative displacement.

## Environment setup

This IOC can be used for any axis, with or without position readback. The environment variables to be set are:

- `SYS` : System name.
- `DEV` : Device name.
- `AXIS_NAME` : The name of the axis.
- `REFLECT_PREFIX` : The PV prefix from the Reflectometer IOC.
- `SET_NAME` : The Reflectometer has different naming convention for different type of axis, e.g. `SetRX` or `setG1T`, where `RX` and `G1T` are the names of specific axes. In this case, this variable must indicate what is the function name (e.g. `Set` or `set`).
- `STATUS_NAME` : The name of the Status PV for a specific axis, without prefix.
- `DONE_VAL` : The value the Status PV should be equal to, when movement is done.
- `RB_PREFIX` : The position readback PV prefix from the Reflectometer IOC. If the axis has a readback, it should be equal to `REFLECT_PREFIX`. Otherwise it should be set to `$(SYS):$(DEV)`.
- `POS_RB_NAME` : The name of the position readback PV, without prefix. If no readback is provided, than it should be set to `SetPointCalc`.

## Run docker

`docker run -dit --name reflectometer --restart always --network host -v "/etc/timezone:/etc/timezone:ro" -v "/etc/localtime:/etc/localtime:ro" -v "./autosave/iocUserPositionerIOC:/opt/epics/autosave/iocUserPositionerIOC" user_positioners_reflectometer:latest`
